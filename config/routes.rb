Rails.application.routes.draw do
  resources :form_contract_values
  resources :form_fields
  resources :fields
  resources :forms
  resources :contracts do
    patch :values_form, on: :member
  end
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
