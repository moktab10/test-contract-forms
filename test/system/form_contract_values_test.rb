require "application_system_test_case"

class FormContractValuesTest < ApplicationSystemTestCase
  setup do
    @form_contract_value = form_contract_values(:one)
  end

  test "visiting the index" do
    visit form_contract_values_url
    assert_selector "h1", text: "Form contract values"
  end

  test "should create form contract value" do
    visit form_contract_values_url
    click_on "New form contract value"

    fill_in "Contract", with: @form_contract_value.contract_id
    fill_in "Form field", with: @form_contract_value.form_field_id
    fill_in "Values", with: @form_contract_value.values
    click_on "Create Form contract value"

    assert_text "Form contract value was successfully created"
    click_on "Back"
  end

  test "should update Form contract value" do
    visit form_contract_value_url(@form_contract_value)
    click_on "Edit this form contract value", match: :first

    fill_in "Contract", with: @form_contract_value.contract_id
    fill_in "Form field", with: @form_contract_value.form_field_id
    fill_in "Values", with: @form_contract_value.values
    click_on "Update Form contract value"

    assert_text "Form contract value was successfully updated"
    click_on "Back"
  end

  test "should destroy Form contract value" do
    visit form_contract_value_url(@form_contract_value)
    click_on "Destroy this form contract value", match: :first

    assert_text "Form contract value was successfully destroyed"
  end
end
