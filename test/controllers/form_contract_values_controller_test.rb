require "test_helper"

class FormContractValuesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @form_contract_value = form_contract_values(:one)
  end

  test "should get index" do
    get form_contract_values_url
    assert_response :success
  end

  test "should get new" do
    get new_form_contract_value_url
    assert_response :success
  end

  test "should create form_contract_value" do
    assert_difference("FormContractValue.count") do
      post form_contract_values_url, params: { form_contract_value: { contract_id: @form_contract_value.contract_id, form_field_id: @form_contract_value.form_field_id, values: @form_contract_value.values } }
    end

    assert_redirected_to form_contract_value_url(FormContractValue.last)
  end

  test "should show form_contract_value" do
    get form_contract_value_url(@form_contract_value)
    assert_response :success
  end

  test "should get edit" do
    get edit_form_contract_value_url(@form_contract_value)
    assert_response :success
  end

  test "should update form_contract_value" do
    patch form_contract_value_url(@form_contract_value), params: { form_contract_value: { contract_id: @form_contract_value.contract_id, form_field_id: @form_contract_value.form_field_id, values: @form_contract_value.values } }
    assert_redirected_to form_contract_value_url(@form_contract_value)
  end

  test "should destroy form_contract_value" do
    assert_difference("FormContractValue.count", -1) do
      delete form_contract_value_url(@form_contract_value)
    end

    assert_redirected_to form_contract_values_url
  end
end
