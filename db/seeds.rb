Contract.create!([
  {name: "Contract 1", subject: "test subject2", form_key_value: "dc4"},
  {name: "Contract 2", subject: "contrat", form_key_value: "dc4"},
  {name: "Contract 3", subject: "test subject", form_key_value: "form_exemple"},
  {name: "Contract 4", subject: "test subject3", form_key_value: "form_exemple"}
])
Form.create!([
  {name: "DC4", settings: "", form_key: "dc4"},
  {name: "Form exemple", settings: "", form_key: "form_exemple"}
])

Field.create!([
  {name: "First name", field_type: "string"},
  {name: "Last name", field_type: "string"},
  {name: "Gender", field_type: "radio_buttons"},
  {name: "Villes", field_type: "check_boxes"},
  {name: "Numbers", field_type: "integer"}
])

FormContractValue.create!([
  {values: {}, contract_id: 1},
  {values: {}, contract_id: 2},
  {values: {}, contract_id: 3},
  {values: {}, contract_id: 4}
])

FormField.create!([
  {key_entity: "contract", settings: "{\"value\": {}, \"label\": \"Last name\", \"placeholder\": \"Tape your last name\", \"multiple\": false, \"required\": false}", field_id: 2, form_id: 1},
  {key_entity: "contract", settings: "{\"value\": {}, \"label\": \"First name\", \"placeholder\": \"Tape your first name\", \"multiple\": false, \"required\": false}", field_id: 1, form_id: 2},
  {key_entity: "contract", settings: "{\"value\": {\"YES\": true, \"NO\": false}, \"label\": \"Gender\", \"placeholder\": \"\", \"multiple\": false, \"required\": false}", field_id: 3, form_id: 2},
  {key_entity: "contract", settings: "{\"value\": {\"Paris\": \"paris\", \"Nice\": \"nice\", \"Marseille\": \"marseille\"}, \"label\": \"Villes\", \"placeholder\": \"\", \"multiple\": false, \"required\": false}", field_id: 4, form_id: 1},
  {key_entity: "contract", settings: "{\"value\": {}, \"label\": \"Numbers\", \"placeholder\": \"\", \"multiple\": false, \"required\": false}", field_id: 5, form_id: 1},
])

