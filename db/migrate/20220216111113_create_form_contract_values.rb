class CreateFormContractValues < ActiveRecord::Migration[7.0]
  def change
    create_table :form_contract_values do |t|
      t.jsonb :values
      t.references :contract, null: false, foreign_key: true

      t.timestamps
    end
  end
end
