class CreateContracts < ActiveRecord::Migration[7.0]
  def change
    create_table :contracts do |t|
      t.string :name
      t.string :status
      t.string :subject
      t.string :form_key_value
      t.string :custom_form_key_value

      t.timestamps
    end
  end
end
