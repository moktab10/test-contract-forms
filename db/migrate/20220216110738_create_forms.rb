class CreateForms < ActiveRecord::Migration[7.0]
  def change
    create_table :forms do |t|
      t.string :name
      t.string :form_key
      t.string :custom_form_key
      t.jsonb :settings, default: {}

      t.timestamps
    end
  end
end
