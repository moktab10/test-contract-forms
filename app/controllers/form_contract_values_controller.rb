class FormContractValuesController < ApplicationController
  before_action :set_form_contract_value, only: %i[ show edit update destroy ]

  # GET /form_contract_values or /form_contract_values.json
  def index
    @form_contract_values = FormContractValue.all
  end

  # GET /form_contract_values/1 or /form_contract_values/1.json
  def show
  end

  # GET /form_contract_values/new
  def new
    @form_contract_value = FormContractValue.new
  end

  # GET /form_contract_values/1/edit
  def edit
  end

  # POST /form_contract_values or /form_contract_values.json
  def create
    @form_contract_value = FormContractValue.new(form_contract_value_params)

    respond_to do |format|
      if @form_contract_value.save
        format.html { redirect_to form_contract_value_url(@form_contract_value), notice: "Form contract value was successfully created." }
        format.json { render :show, status: :created, location: @form_contract_value }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @form_contract_value.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /form_contract_values/1 or /form_contract_values/1.json
  def update
    respond_to do |format|
      if @form_contract_value.update(form_contract_value_params)
        format.html { redirect_to form_contract_value_url(@form_contract_value), notice: "Form contract value was successfully updated." }
        format.json { render :show, status: :ok, location: @form_contract_value }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @form_contract_value.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /form_contract_values/1 or /form_contract_values/1.json
  def destroy
    @form_contract_value.destroy

    respond_to do |format|
      format.html { redirect_to form_contract_values_url, notice: "Form contract value was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_form_contract_value
      @form_contract_value = FormContractValue.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def form_contract_value_params
      params.require(:form_contract_value).permit(:values, :contract_id)
    end
end
