json.extract! form_field, :id, :level, :key_entity, :settings, :field_id, :form_id, :created_at, :updated_at
json.url form_field_url(form_field, format: :json)
