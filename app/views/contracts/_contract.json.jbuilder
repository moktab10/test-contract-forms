json.extract! contract, :id, :status, :subject, :form_contract_key, :created_at, :updated_at
json.url contract_url(contract, format: :json)
