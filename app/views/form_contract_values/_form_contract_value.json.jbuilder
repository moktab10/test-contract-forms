json.extract! form_contract_value, :id, :values, :contract_id, :created_at, :updated_at
json.url form_contract_value_url(form_contract_value, format: :json)
