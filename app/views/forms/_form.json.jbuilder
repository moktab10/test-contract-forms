json.extract! form, :id, :name, :form_key, :settings, :created_at, :updated_at
json.url form_url(form, format: :json)
