class Field < ApplicationRecord
    has_many :form_fields, dependent: :destroy
end
