class Contract < ApplicationRecord
    has_one :form_contract_value, dependent: :destroy
end
