class FormField < ApplicationRecord
  belongs_to :field
  belongs_to :form
end
