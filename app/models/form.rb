class Form < ApplicationRecord
  has_many :form_fields, dependent: :destroy
  has_many :fields, through: :form_fields
end
